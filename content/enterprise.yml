---
  title: GitLab for Enterprise - Collaboration made easy
  description: Accelerate Enterprise software delivery with the GitLab DevOps Platform, lowering your development costs and streamlining team collaboration
  image_title: /nuxt-images/open-graph/open-graph-enterprise.png
  solutions_hero:
    title: GitLab for Enterprises
    subtitle: One DevOps Platform, from planning to production. Collaborate across your organization, ship secure code faster, and drive business results.
    header_animation: fade-down
    header_animation_duration: 800
    buttons_animation: fade-down
    buttons_animation_duration: 1200
    img_animation: zoom-out-left
    img_animation_duration: 1600
    primary_btn:
      text: Join GitLab
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: join gitlab
      data_ga_location: header
    secondary_btn:
      text: Try for Free
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: try for free
      data_ga_location: header
    image:
      image_url: /nuxt-images/enterprise/enterprise-header.jpeg
      alt: ""
      rounded: true
  by_industry_intro:
    logos:
      - name: Siemens
        image: /nuxt-images/customers/logo_siemens_color.svg
        aos_animation: zoom-in-up
        aos_duration: 200
        url: /customers/siemens/
        aria_label: Link to Siemens customer case study
      - name: hilti
        image: /nuxt-images/customers/hilti-logo.png
        aos_animation: zoom-in-up
        aos_duration: 400
        url: /customers/hilti/
        aria_label: Link to Hilti customer case study
      - name: Bendigo
        image: /nuxt-images/customers/babl_logo.png
        aos_animation: zoom-in-up
        aos_duration: 600
        url: /customers/bab/
        aria_label: Link to Bendigo customer case study
      - name: Radio France
        image: /nuxt-images/customers/logoradiofrance.svg
        aos_animation: zoom-in-up
        aos_duration: 800
        url: /customers/radiofrance/
        aria_label: Link to Radio France customer case study
      - name: Credit agricole
        image: /nuxt-images/customers/credit-agricole-logo-1.png
        aos_animation: zoom-in-up
        aos_duration: 1000
        url: /customers/credit-agricole/
        aria_label: Link to Credit Agricole customer case study
      - name: Kiwi
        image: /nuxt-images/customers/kiwi.png
        aos_animation: zoom-in-up
        aos_duration: 1200
        url: /customers/kiwi/
        aria_label: Link to Kiwi customer case study
  by_solution_intro:
    aos_animation: fade-up
    aos_duration: 800
    text:
      highlight: Enterprises rely on DevOps to innovate, modernize, and accelerate.
      description: What worked well for individual projects will be difficult to scale across the enterprise. DevOps solutions shouldn’t create more problems than they solve. Unlike brittle toolchains built on point solutions, GitLab lets teams iterate faster and innovate together, removing complexity and risk, providing everything you need to deliver higher quality, more secure software faster.
  by_solution_benefits:
    title: DevOps at scale
    is_accordion: true
    right_block_animation: zoom-in-left
    right_block_duration: 800
    left_block_animation: zoom-in-right
    left_block_duration: 800
    header_animation: fade-up
    header_duration: 800
    image:
      image_url: /nuxt-images/enterprise/enterprise-devops-at-scale.jpg
      alt: ""
    items:
      - icon: /nuxt-images/solutions/benefits/increase.svg
        header: Collaborate more productively
        text: Eliminate click-ops, introduce checks and balances essential for cloud native adoption.
      - icon: /nuxt-images/solutions/benefits/vc-and-c.svg
        header: Reduce risk and cost
        text: More testing, errors detected earlier, less risk.
      - icon: /nuxt-images/solutions/benefits/collaberation.svg
        header: Deliver better software faster
        text: Minimize repetitive tasks, focus on value generating tasks.
      - icon: /nuxt-images/solutions/benefits/agile.svg
        header: Simplify DevOps
        text: Manage all of your DevOps processes in one place letting you scale your success without scaling your complexity.
  by_industry_solutions_block:
    subtitle: A complete DevOps platform for Public Sector
    sub_description: "Starting with one DevOps platform that includes secure and robust source code management (SCM), continuous integration (CI), continuous delivery (CD), and continuous software security and compliance, GitLab addresses your unique needs such as these:"
    white_bg: true
    sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
    solutions:
      - title: Agile planning
        description: Plan, initiate, prioritize, and manage innovation initiatives, with complete visibility and connection into the work being done.
        icon: /nuxt-images/solutions/benefits/agile.svg
        link_text: Learn More
        link_url: /solutions/agile-delivery
        data_ga_name: agile planning
        data_ga_location: body
      - title: Automated Software Delivery
        description: Review your project’s software bill of materials with key details about the dependencies used, including their known vulnerabilities.
        icon: /nuxt-images/solutions/automated-code.svg
        link_text: Learn More
        link_url: /solutions/delivery-automation/
        data_ga_name: Automated Software Delivery
        data_ga_location: body
      - title: Continuous Security & Compliance
        description: Shift security left and automate compliance throughout the development process to reduce risk and delays.
        icon: /nuxt-images/solutions/benefits/devsecops-loop.svg
        link_text: Learn More
        link_url: /solutions/continuous-software-compliance/
        data_ga_name: Continuous Security & Compliance
        data_ga_location: body
      - title: Value Stream Management
        description: Provide actionable insight to every stakeholder in the organization, with visibility into every stage of ideation and development.
        icon: /nuxt-images/solutions/visibility.svg
        link_text: Learn More
        link_url: /solutions/value-stream-management
        data_ga_name: Value Stream Management
        data_ga_location: body
      - title: Reliability
        description: Geographically distributed teams use Geo to provide a fast and efficient experience across the globe with a warm-standby as part of a disaster recovery strategy.
        icon: /nuxt-images/solutions/remote-world.svg
      - title: High availability - at scale
        description: Reference architecture for high availability over 50,000 users
        icon: /nuxt-images/solutions/auto-scale.svg
        link_text: Learn More
        link_url: https://docs.gitlab.com/ee/administration/reference_architectures/
        data_ga_name: High availability
        data_ga_location: body
  by_solution_value_prop:
    title: One Platform for Dev, Sec, and Ops
    header_animation: fade-up
    header_animation_duration: 500
    cards_animation: zoom-in-up
    cards_animation_duration: 500
    cards:
      - title: Comprehensive
        description: Visualize and optimize your entire DevOps lifecycle with platform-wide analytics within the same system where you do your work.
        icon: /nuxt-images/icons/value-prop/less-risk.svg
      - title: DevOps simplified
        description: Use a common set of tools across teams and lifecycle stages, without dependencies on third-party plugins or APIs that can disrupt your workflow.
        icon: /nuxt-images/icons/value-prop/devsecops.svg
      - title: Secure
        description: Scan for vulnerabilities and compliance violations with each commit.
        icon: /nuxt-images/icons/value-prop/eye-magnifying-glass.svg
      - title: Transparent and compliant
        description: Automatically capture and correlate all actions—from planning to code changes to approvals—for easy traceability during audits or retrospectives.
        icon: /nuxt-images/icons/value-prop/monitor-web-app.svg
      - title: Easy to scale
        description: Reference architectures show you how to scale high availability for installations with more than 50,000 users.
        icon: /nuxt-images/icons/value-prop/monitor-web-app.svg
      - title: Scalable
        description: Deploy GitLab onto a Kubernetes cluster and horizontally scale. No downtime on upgrades. Use GitOps workflow or CI/CD workflow.
        icon: /nuxt-images/icons/value-prop/monitor-web-app.svg
  by_industry_case_studies:
    title: Customer Realized Benefits
    charcoal_bg: true
    header_animation: fade-up
    header_animation_duration: 500
    row_animation: fade-right
    row_animation_duration: 800
    rows:
      - title: Siemens
        subtitle: How Siemens created an open source DevOps culture with GitLab
        image:
          url: /nuxt-images/blogimages/siemenscoverimage_casestudy.jpg
          alt: Soldier speaking on radio
        button:
          href: /customers/siemens/
          text: Learn more
          data_ga_name: siemens learn more
          data_ga_location: body
      - title: Hilti
        subtitle: How CI/CD and robust security scanning accelerated Hilti’s SDLC
        image:
          url: /nuxt-images/blogimages/hilti_cover_image.jpg
          alt: Houses as seen from above
        button:
          href: /customers/hilti/
          text: Learn more
          data_ga_name: hilti learn more
          data_ga_location: body
      - title: Bendigo
        subtitle: Learn how GitLab is accelerating DevOps at Bendigo and Adelaide Bank
        image:
          url: /nuxt-images/blogimages/bab_cover_image.jpg
          alt: University of Washington Campus
        button:
          href: /customers/bab/
          text: Learn more
          data_ga_name: bendigo learn more
          data_ga_location: body
      - title: Radio France
        subtitle: Radio France deploys 5x faster with GitLab CI/CD
        image:
          url: /nuxt-images/blogimages/radio-france-cover-image.jpg
          alt: University of Washington Campus
        button:
          href: /customers/radiofrance/
          text: Learn more
          data_ga_name: radio france learn more
          data_ga_location: body
  solutions_resource_cards:
    column_size: 4
    cards:
      - icon: /nuxt-images/features/resources/icon_webcast.svg
        event_type: Webinar
        header: Deliver more value with fewer headaches using an end-to-end DevOps platform
        link_text: Watch now
        image: /nuxt-images/features/resources/resources_waves.png
        href: https://www.youtube.com/watch?v=wChaqniv3HI
        aos_animation: fade-up
        aos_duration: 400
      - icon: /nuxt-images/features/resources/icon_webcast.svg
        event_type: Webinar
        header: DevOps Platform technical demo
        link_text: Watch now
        image: /nuxt-images/features/resources/resources_webcast.png
        href: https://youtu.be/Oei67XCnXMk
        aos_animation: fade-up
        aos_duration: 600
      - icon: /nuxt-images/features/resources/icon_event.svg
        event_type: Virtual Event
        header: Northwestern Mutual’s Digital Transformation with GitLab
        link_text: Watch now
        image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
        href: https://www.youtube.com/watch?v=o6EY_WwEFpE
        aos_animation: fade-up
        aos_duration: 800
      - icon: /nuxt-images/features/resources/icon_event.svg
        event_type: Virtual Event
        header: The Next Iteration of DevOps (CEO Keynote)
        link_text: Watch now
        image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
        href: https://www.youtube.com/watch?v=Wx8tDVSeidk
        aos_animation: fade-up
        aos_duration: 1000
      - icon: /nuxt-images/features/resources/icon_case_study.svg
        event_type: Case Study
        header: Goldman Sachs improves from 1 build every two weeks to over a thousand per day
        link_text: Read more
        image: /nuxt-images/features/resources/resources_case_study.png
        href: /customers/goldman-sachs/
        data_ga_name: Goldman Sachs
        data_ga_location: body
        aos_animation: fade-up
        aos_duration: 1200
      - icon: /nuxt-images/features/resources/icon_video.svg
        event_type: Video
        header: GitLab Infomercial
        link_text: Watch now
        image: /nuxt-images/features/resources/resources_golden_dog.png
        href: https://www.youtube.com/watch?v=gzYTZhJlHoI
        aos_animation: fade-up
        aos_duration: 1400
